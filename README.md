<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>
-->

# Description

Sentry Integration service that bridges sentry's issues capabilities to bugzilla.

<https://docs.sentry.io/product/integrations/integration-platform/ui-components/issue-link>
