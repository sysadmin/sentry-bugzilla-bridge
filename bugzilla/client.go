// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

package bugzilla

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"net/url"

	"invent.kde.org/sysadmin/sentry-bugzilla-bridge/app"
)

type connection interface {
	get(url string) []byte
	put(url string, body []byte)
}

type httpConnection struct {
	client *http.Client
}

type Bug struct {
	SentryUrl string `json:"cf_sentryurl"`
}

type BugResponse struct {
	Bugs []Bug `json:"bugs"`
}

func (c *httpConnection) get(url string) []byte {
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		log.Println(err)
		return nil
	}
	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
		return nil
	}
	return body
}

func (c *httpConnection) put(urlString string, putBody []byte) {
	url, err := url.Parse(urlString)
	if err != nil {
		log.Println(err)
		return
	}
	urlQuery := url.Query()
	urlQuery.Add("Bugzilla_api_key", app.Conf.BugzillaAPIKey)
	url.RawQuery = urlQuery.Encode()

	request, err := http.NewRequest(http.MethodPut, url.String(), bytes.NewReader(putBody))
	if err != nil {
		log.Println(err)
		return
	}
	request.Header.Add("Content-Type", "application/json")

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		log.Println(err)
		return
	}
	body, err := io.ReadAll(response.Body)
	log.Println(string(body))
	defer response.Body.Close()
}

type Client struct {
	connection connection
}

var DefaultHost = "https://bugstest.kde.org"

func NewClient() *Client {
	return &Client{connection: &httpConnection{}}
}

func (c *Client) UpdateSentryUrl(bugID string, sentryUrl string) error {
	body := c.connection.get(DefaultHost + "/rest/bug/" + bugID + "?include_fields=_custom")
	if body == nil {
		return errors.New("failed to get bug report fields")
	}
	bugs := BugResponse{}
	json.Unmarshal(body, &bugs)

	if len(bugs.Bugs) != 1 {
		return errors.New("Unexpectedly found not exactly one bug as result to " + bugID)
	}

	bug := bugs.Bugs[0]
	if bug.SentryUrl != "" {
		return errors.New("Bug already has a sentry url attached " + bugID + " sentry " + bug.SentryUrl)
	}

	putBody, err := json.Marshal(Bug{SentryUrl: sentryUrl})
	if err != nil {
		return err
	}
	c.connection.put(DefaultHost+"/rest/bug/"+bugID, putBody)

	return nil
}
