// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

package bugzilla

import (
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"runtime"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLink(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, filename, _, _ := runtime.Caller(0)
		switch r.URL.Path {
		case "/rest/bug/452962":
			switch r.Method {
			case http.MethodGet:
				data, err := os.ReadFile(path.Join(path.Dir(filename), "452962.json"))
				if err != nil {
					t.Error(err)
					return
				}

				w.WriteHeader(http.StatusOK)
				w.Write(data)
				return
			case http.MethodPut:
				body, err := io.ReadAll(r.Body)
				if err != nil {
					t.Error(err)
					return
				}
				defer r.Body.Close()

				assert.Equal(t, string(body), `{"cf_sentryurl":"https://crash-reports.kde.org/organizations/kde/issues/37032/events/3afa4c8e414b4e9ea95fb2bf8f6e230c/"}`)

				w.WriteHeader(http.StatusOK)
				return
			}
		}

		t.Errorf("Unexpected request %+v", r)
	}))
	defer server.Close()

	DefaultHost = server.URL
	c := NewClient()
	err := c.UpdateSentryUrl(strconv.FormatInt(452962, 10), "https://crash-reports.kde.org/organizations/kde/issues/37032/events/3afa4c8e414b4e9ea95fb2bf8f6e230c/")
	assert.NoError(t, err)
}

func TestExists(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, filename, _, _ := runtime.Caller(0)
		switch r.URL.Path {
		case "/rest/bug/452963":
			switch r.Method {
			case http.MethodGet:
				data, err := os.ReadFile(path.Join(path.Dir(filename), "452963.json"))
				if err != nil {
					t.Error(err)
					return
				}

				w.WriteHeader(http.StatusOK)
				w.Write(data)
				return
			}
		}

		t.Errorf("Unexpected request %+v", r)
	}))
	defer server.Close()

	DefaultHost = server.URL
	c := NewClient()
	err := c.UpdateSentryUrl(strconv.FormatInt(452963, 10), "https://crash-reports.kde.org/organizations/kde/issues/37032/events/3afa4c8e414b4e9ea95fb2bf8f6e230c/")
	assert.ErrorContains(t, err, "Bug already has a sentry url attached")
}
