// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018-2020 Harald Sitter <sitter@kde.org>

package app

import (
	"os"
	"path"
	"path/filepath"
	"runtime"
	"testing"

	yaml "gopkg.in/yaml.v2"
)

// Conf is the global configuration object of the app configuration.
var Conf = loadConfiguration()

// Config encapsulates the deserialized app configuration.
type Config struct {
	// sentry dsn
	DSN string `yaml:"dsn"`
	// sentry integration installation id
	InstallationID string `yaml:"installationId"`
	// Our deploy token. Must be provided for the update endpoint
	DeployToken string `yaml:"deployToken"`
	// Actual user API token to facilitate automatic linking
	// Must have event:admin, event:read, org:read, project:read
	APIToken string `yaml:"apiToken"`
	// Bugzilla API token
	BugzillaAPIKey string `yaml:"bugzillaApiKey"`
}

func loadConfiguration() *Config {
	configPath := filepath.Join(os.Getenv("HOME"), ".config/sentry-bugzilla-bridge.yaml")

	if testing.Testing() {
		_, filename, _, _ := runtime.Caller(0)
		configPath = path.Join(path.Dir(filename), "../config.example.yaml")
	}

	yamlFile, err := os.ReadFile(configPath)
	if err != nil {
		panic("missing ~/.config/sentry-bugzilla-bridge.yaml")
	}

	y := &Config{}
	err = yaml.Unmarshal(yamlFile, y)
	if err != nil {
		panic(err)
	}

	return y
}
