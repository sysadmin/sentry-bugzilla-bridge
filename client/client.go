// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"
	"strings"

	"invent.kde.org/sysadmin/sentry-bugzilla-bridge/app"
)

type connection interface {
	get(url string) [][]byte
	post(url string, body []byte)
}

type httpConnection struct {
	client *http.Client
}

type Issue struct {
	ID        string `json:"id"`
	Permalink string `json:"permalink"`
}

type IssueLink struct {
	ServiceType string `json:"serviceType"`
}

type CreateIssueLink struct {
	GroupID   string `json:"groupId"`
	Action    string `json:"action"`
	URI       string `json:"uri"`
	BugNumber int64  `json:"bug_number"`
}

type UserFeedback struct {
	Comments string `json:"comments"`
	EventID  string `json:"eventID"`
	Issue    Issue  `json:"issue"`
}

func (c *httpConnection) get(url string) [][]byte {
	bodies := [][]byte{}

	link := Link{Next: Cursor{URL: url, Results: true}}
	for {
		if !link.Next.Results { // end of link
			break
		}

		request, err := http.NewRequest(http.MethodGet, link.Next.URL, nil)
		if err != nil {
			return nil
		}
		request.Header.Set("Authorization", fmt.Sprintf("Bearer %v", app.Conf.APIToken))

		response, err := http.DefaultClient.Do(request)
		if err != nil {
			log.Println(err)
			return nil
		}
		defer response.Body.Close()

		link = ParseLink(response.Header.Get("Link"))

		body, err := io.ReadAll(response.Body)
		if err != nil {
			log.Println(err)
			return nil
		}

		bodies = append(bodies, body)
	}

	return bodies
}

func (c *httpConnection) post(url string, postBody []byte) {
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(postBody))
	if err != nil {
		fmt.Println(err)
		return
	}
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %v", app.Conf.APIToken))
	request.Header.Add("Content-Type", "application/json")

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		log.Println(err)
		return
	}
	body, err := io.ReadAll(response.Body)
	fmt.Println(string(body))
	defer response.Body.Close()
}

type Client struct {
	connection connection
}

func NewClient() *Client {
	return &Client{connection: &httpConnection{}}
}

type Cursor struct {
	URL     string
	Results bool
}

type Link struct {
	Next Cursor
}

func ParseLink(header string) Link {
	// Link:[<https://errors-eval.kde.org/api/0/organizations/kde/user-feedback/?&cursor=1684705065390:0:1>; rel="previous"; results="false"; cursor="1684705065390:0:1",
	//       <https://errors-eval.kde.org/api/0/organizations/kde/user-feedback/?&cursor=1684705065391:100:0>; rel="next"; results="false"; cursor="1684705065391:100:0"]

	if header == "" {
		return Link{Next: Cursor{URL: "/dev/null", Results: false}}
	}

	link := Link{}

	r, _ := regexp.Compile("<([^>]+)>; rel=\"([^\"]+)\"; results=\"([^\"]+)\"")

	links := strings.SplitN(header, ",", 2)
	for _, page := range links {
		m := r.FindStringSubmatch(page)
		url := m[1]
		rel := m[2]
		results := m[3]

		if rel != "next" {
			continue
		}

		link.Next = Cursor{URL: url, Results: results == "true"}
	}

	return link
}

func (c *Client) OrganizationUserFeedback() []UserFeedback {
	ret := []UserFeedback{}

	for _, body := range c.connection.get("https://crash-reports.kde.org/api/0/organizations/kde/user-feedback/?statsPeriod=6h") {
		feedback := []UserFeedback{}
		json.Unmarshal(body, &feedback)
		ret = append(ret, feedback...)
	}

	return ret
}

func (c *Client) IssueLinks(issueID string) []IssueLink {
	ret := []IssueLink{}
	for _, body := range c.connection.get("https://crash-reports.kde.org/api/0/groups/" + issueID + "/external-issues/") {
		link := []IssueLink{}
		json.Unmarshal(body, &link)
		ret = append(ret, link...)
	}
	return ret
}

func (c *Client) LinkIssue(link CreateIssueLink) {
	body, err := json.Marshal(link)
	if err != nil {
		fmt.Println(err)
		return
	}
	c.connection.post("https://crash-reports.kde.org/api/0/sentry-app-installations/"+app.Conf.InstallationID+"/external-issue-actions/", body)
}
