// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018-2023 Harald Sitter <sitter@kde.org>

package main

import (
	"archive/zip"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"regexp"
	"runtime/debug"
	"strconv"
	"syscall"
	"time"

	"github.com/coreos/go-systemd/activation"
	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"invent.kde.org/sysadmin/sentry-bugzilla-bridge/app"
	"invent.kde.org/sysadmin/sentry-bugzilla-bridge/bugzilla"
	"invent.kde.org/sysadmin/sentry-bugzilla-bridge/client"
	docs "invent.kde.org/sysadmin/sentry-bugzilla-bridge/docs"
	"invent.kde.org/sysadmin/sentry-bugzilla-bridge/models"
)

//go:generate swag init --codeExampleFiles ./examples

// Link sentry issues to bugzilla issues. This is all sorts of awkward because none of the existing sentry APIs appear
// to be flexible enough to let us make our fairly out of the ordinary requests. So instead I've implemented the
// dumbest client I can get away with solely so we can do bug linking.
func linker() {
	sentry := client.NewClient()
	bugzilla.DefaultHost = "https://bugs.kde.org"
	bugzilla := bugzilla.NewClient()

	r, _ := regexp.Compile(`https://bugs.kde.org/show_bug.cgi\?id=(\d+)`)

	for _, feedback := range sentry.OrganizationUserFeedback() {
		m := r.FindStringSubmatch(feedback.Comments)
		if len(m) < 2 {
			continue
		}

		bugID, err := strconv.ParseInt(m[1], 10, 0)
		if err != nil {
			fmt.Println(err)
			continue
		}

		bugzilla.UpdateSentryUrl(strconv.FormatInt(bugID, 10), feedback.Issue.Permalink+"events/"+feedback.EventID+"/")

		links := sentry.IssueLinks(feedback.Issue.ID)
		alreadyLinked := false
		for _, link := range links {
			if link.ServiceType == "sitter-32fa35" {
				alreadyLinked = true
				break
			}
		}
		if alreadyLinked {
			continue
		}

		sentry.LinkIssue(client.CreateIssueLink{GroupID: feedback.Issue.ID, Action: "link", URI: `/sentry/issues/link`, BugNumber: bugID})
	}
}

func sentryHeartbeat() {
	id := sentry.CaptureCheckIn(
		&sentry.CheckIn{
			MonitorSlug: "sentry-bugzilla-bridge",
			Status:      sentry.CheckInStatusOK,
		},
		nil,
	)
	if id == nil {
		fmt.Println("Unexpectedly failed to get checkin id")
	}
}

func sentryTransactor() gin.HandlerFunc {
	return func(c *gin.Context) {
		span := sentry.StartSpan(c, c.Request.Method, sentry.WithTransactionName(c.Request.URL.Path))
		c.Next()
		span.Finish()
	}
}

func version() string {
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			if setting.Key == "vcs.revision" {
				return setting.Value
			}
		}
	}
	return ""
}

func uri(c *gin.Context) string {
	url := url.URL{}
	url.Scheme = "http"
	url.Host = c.Request.Host
	return url.String()
}

// godoc
// @Summary issues
// @Description Get all bugzilla bugs matching a query
// @Description https://docs.sentry.io/product/integrations/integration-platform/ui-components/formfield#uri-request-format
// @Param installationId path string true "Sentry integration ID"
// @Param projectSlug path string true "Sentry project slug"
// @Param query path string true "query - free form string fed into the bugzilla search"
// @Router /sentry/issues [get]
func issues(c *gin.Context) {
	installationID := c.Query("installationId")
	if installationID != app.Conf.InstallationID {
		c.String(http.StatusForbidden, "")
		return
	}
	projectSlug := c.Query("projectSlug")
	if projectSlug == "" {
		c.String(http.StatusNotFound, "Missing projectSlug")
		return
	}
	query := c.Query("query")
	if query == "" {
		c.String(http.StatusNotFound, "Missing query")
		return
	}

	// "https://bugs.kde.org/rest/bug?quicksearch="
	url, err := url.Parse("https://bugs.kde.org/rest/bug")
	if err != nil {
		log.Fatal(err)
		return
	}
	urlQuery := url.Query()
	urlQuery.Add("quicksearch", query)
	url.RawQuery = urlQuery.Encode()
	resp, err := http.Get(url.String())
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		log.Print("unexpected response status", resp.Status, body)
		return
	}
	var f interface{}
	err = json.Unmarshal(body, &f)
	if err != nil {
		log.Fatal(err)
		return
	}
	m := f.(map[string]interface{})

	ret := []gin.H{}

	if val, ok := m["bugs"]; ok {
		bugFields := val.([]interface{})
		for _, bugField := range bugFields {
			bug := bugField.(map[string]interface{})
			ret = append(ret, gin.H{"label": bug["summary"], "value": bug["id"], "default": false})
		}
	}

	c.JSON(http.StatusOK, ret)
}

// godoc
// @Summary link
// @Description Links a sentry issue to bugzilla
// @Description https://docs.sentry.io/product/integrations/integration-platform/ui-components/issue-link/#request-format
// @Param request body models.Request true "query params"
// @Router /sentry/issues/link [post]
func link(c *gin.Context) {
	r := models.Request{}
	body, err := io.ReadAll(c.Request.Body)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(body, &r)

	if r.InstallationID != app.Conf.InstallationID {
		c.String(http.StatusForbidden, "")
		return
	}

	c.JSON(http.StatusOK, models.Response{WebUrl: fmt.Sprintf("https://bugs.kde.org/show_bug.cgi?id=%v", r.Fields["bug_number"]),
		Project: "Bugzilla", Identifier: fmt.Sprintf("%v", r.Fields["bug_number"])})
}

func download(dir string) string {
	resp, err := http.Get("https://invent.kde.org/sysadmin/sentry-bugzilla-bridge/-/jobs/artifacts/master/download?job=build")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	target := dir + "/archive.zip"
	out, _ := os.Create(target)
	defer out.Close()
	io.Copy(out, resp.Body)
	return target
}

func unzip(path string, target string) {
	reader, _ := zip.OpenReader(path)
	defer reader.Close()

	found := false
	for _, file := range reader.File {
		if file.Name == "sentry-bugzilla-bridge" {
			found = true
			in, err := file.Open()
			if err != nil {
				panic(err)
			}
			defer in.Close()

			out, err := os.Create(target)
			if err != nil {
				panic(err)
			}
			defer out.Close()

			io.Copy(out, in)
			break
		}
	}
	if !found {
		panic("Failed to find artifact")
	}
}

// godoc
// @Summary update
// @Description Auto-updates the service. A deploy token must be provided as Authorization header.
// @Security DeployToken
// @Router /update [post]
func update(c *gin.Context) {
	if c.GetHeader("Authorization") != fmt.Sprintf("DeployToken %v", app.Conf.DeployToken) {
		c.String(http.StatusUnauthorized, "")
		return
	}

	exe, err := os.Executable()
	if err != nil {
		panic(err)
	}

	dir, err := os.MkdirTemp("", "prefix")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(dir)

	archive := download(dir)
	if archive == "" {
		panic("Failed to download")
	}

	pendingExe := exe + ".pending"
	unzip(archive, pendingExe)
	os.Chmod(pendingExe, 0755)
	err = os.Rename(pendingExe, exe)
	if err != nil {
		log.Fatal(err)
	}

	// Terminate. Systemd will restart us with the new binary.
	syscall.Kill(syscall.Getpid(), syscall.SIGTERM)
}

// @title Sentry Bugzilla Bridge
// @version 1.0
// @description Integration for sentry that bridges to bugzilla
//
// @contact.name Source
// @contact.url https://invent.kde.org/sysadmin/sentry-bugzilla-bridge
//
// @license.name AGPL-3.0-or-later
// @license.url https://spdx.org/licenses/AGPL-3.0-or-later.html
//
// @securityDefinitions.apikey DeployToken
// @in header
// @name Authorization
//
// @host sentry-bugzilla-bridge.crash-reports.kde.org
// @BasePath /
// @query.collection.format multi
func main() {
	flag.Parse()

	if err := sentry.Init(sentry.ClientOptions{
		Dsn:                app.Conf.DSN,
		TracesSampleRate:   0.25,
		AttachStacktrace:   true,
		Release:            version(),
		Debug:              true,
		EnableTracing:      true,
		ProfilesSampleRate: 0.25,
	}); err != nil {
		fmt.Printf("Sentry initialization failed: %v\n", err)
	}
	defer sentry.Flush(2 * time.Second)

	heartbeat := time.NewTicker(30 * time.Minute)
	go func() {
		for {
			select {
			case <-heartbeat.C:
				sentryHeartbeat()
			}
		}
	}()

	linkerTimer := time.NewTicker(5 * time.Minute)
	go func() {
		linker()
		for {
			select {
			case <-linkerTimer.C:
				linker()
			}
		}
	}()

	docs.SwaggerInfo.BasePath = "/"

	log.Println("Ready to rumble...")
	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.Use(sentrygin.New(sentrygin.Options{Repanic: true}))
	router.Use(sentryTransactor())
	router.SetTrustedProxies([]string{"127.0.0.1"})

	router.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusFound, "/swagger/index.html")
	})
	router.GET("/doc", func(c *gin.Context) {
		c.Redirect(http.StatusFound, "/swagger/index.html")
	})
	router.GET("/docs", func(c *gin.Context) {
		c.Redirect(http.StatusFound, "/swagger/index.html")
	})
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	router.GET("/sentry/issues", issues)
	router.POST("/sentry/issues/link", link)
	router.POST("/v1/update", update)
	router.GET("/v1/version", func(c *gin.Context) {
		c.String(http.StatusOK, version())
	})

	listeners, err := activation.Listeners()
	if err != nil {
		panic(err)
	}

	log.Println("starting servers")
	var servers []*http.Server
	for _, listener := range listeners {
		server := &http.Server{Handler: router}
		go server.Serve(listener)
		servers = append(servers, server)
	}

	if len(servers) == 0 {
		panic("not started by systemd socket activation")
	}

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	// Wait for some quit cause.
	// This could be INT, TERM, QUIT or the db update trigger.
	// We'll then do a zero downtime shutdown.
	// This relies on systemd managing the socket and us doing graceful listener
	// shutdown. Once we are no longer listening, the system starts backlogging
	// the socket until we get restarted and listen again.
	// Ideally this results in zero dropped connections.
	<-quit
	log.Println("servers are shutting down")

	for _, srv := range servers {
		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
		defer cancel()
		srv.SetKeepAlivesEnabled(false)
		if err := srv.Shutdown(ctx); err != nil {
			log.Fatalf("Server Shutdown: %s", err)
		}
	}

	log.Println("Server exiting")
}
