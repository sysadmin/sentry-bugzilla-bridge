# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

all: sentry-bugzilla-bridge

clean:
	rm -rf sentry-bugzilla-bridge

install:
	systemctl --user stop sentry-bugzilla-bridge.service || true
	go install
	cp -rv systemd/* ~/.config/systemd/user/
	systemctl --user daemon-reload
	systemctl --user enable sentry-bugzilla-bridge.socket
	systemctl --user restart sentry-bugzilla-bridge.socket

test:
	go test -v -coverpkg=./... -coverprofile=coverage.cov ./...

sentry-bugzilla-bridge:
	go install github.com/swaggo/swag/cmd/swag@latest
	go generate
	go build -o sentry-bugzilla-bridge -v

run: sentry-bugzilla-bridge
	/usr/bin/systemd-socket-activate -l 0.0.0.0:8080 ./sentry-bugzilla-bridge

