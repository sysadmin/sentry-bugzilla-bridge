// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package models

type Project struct {
	Slug string `json:"slug"`
	ID   string `json:"id"`
}

type Actor struct {
	Type string `json:"type"`
	Name string `json:"name"`
	IDs  string `json:"id"`
}

type Request struct {
	Fields         map[string]interface{} `json:"fields"`
	InstallationID string                 `json:"installationId"`
	IssueID        string                 `json:"issueId"`
	WebURL         string                 `json:"webUrl"`
	Project        Project                `json:"project"`
	Actor          Actor                  `json:"actor"`
}

type Response struct {
	WebUrl     string `json:"webUrl"`
	Project    string `json:"project"`
	Identifier string `json:"identifier"`
}
